#!/bin/bash

echo "cloning iss2020Robot\n"

mkdir iss2020Robot

cd iss2020Robot

git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/waiter.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/robotWeb2020.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/tearoom.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/it.unibo.qakactor.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/utils.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/basicrobot.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/frpiano/uniboino.git && \
git clone https://fpdevel:iss2020robot!@gitlab.com/methiu88/virtualrobot.git 
