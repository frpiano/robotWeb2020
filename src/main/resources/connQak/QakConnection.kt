package connQak

class QakConnection( host : String, port : String, qakdest : String, ctxqadest : String) {
	
	var host : String = ""
	var port : String = ""
	var qakdest : String = ""
	var ctxqadest : String = ""
	init {
		this.host = host
		this.port = port
		this.qakdest = qakdest
		this.ctxqadest = ctxqadest
	}
	
	override fun toString() : String{
		return host + " " + port + " " + qakdest + " " + ctxqadest
	}
}