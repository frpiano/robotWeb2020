package connQak

import org.json.JSONObject
import java.io.File
import java.net.InetAddress
import java.util.HashMap

//import java.nio.charset.Charset
//import org.apache.commons.io.Charsets


object configurator {
	//Page
	@JvmStatic
	var defaultPage = "tearoomGui"
	@JvmStatic
	var robotStep = "355"

	//MQTT broker	
//	@JvmStatic var mqtthostAddr    	= "broker.hivemq.com"
	@JvmStatic
	var mqtthostAddr = "localhost"
	@JvmStatic
	var mqttport = "1883"
	
	@JvmField val HOST = "host"
	@JvmField val PORT = "port"
	@JvmField val QAKDEST = "qakdest"
	@JvmStatic val CTXQADEST = "ctxqadest"

	@JvmStatic
	var actorMap = hashMapOf<String, QakConnection>()

	@JvmStatic    //to be used by Java
	fun configure() {
		try {
			val configfile = File("connectionConfig.json")
			
			if(!configfile.exists()){
				throw IllegalStateException("file connectionConfig.json not found")
			}
			val config = configfile.readText()    //charset: Charset = Charsets.UTF_8
			//println( "		--- configurator | config=$config" )
			val jsonObject = JSONObject(config)
			defaultPage = jsonObject.getString("defaultPage")
			robotStep = jsonObject.getString("robotStep")
			
			val actors = jsonObject.getJSONObject("actors")
			
			val keys = actors.keys();
			
			for(key in keys){
				
				val actor = actors!!.getJSONObject(key.toString());
				
				val host = actor.get(HOST)!!.toString()
				val port = actor.get(PORT)!!.toString()
				val qakdest = actor.get(QAKDEST)!!.toString()
				val ctxdest = actor.get(CTXQADEST).toString()
				
				var conn = QakConnection(host, port, qakdest, ctxdest)
				
				actorMap.put(key.toString(), conn)
			}
		} catch (ise : IllegalStateException){
			System.out.println(ise.message)
		} catch (e: Exception) {
			System.out.println(" There was a problem parsing connectionConfig.json ")
		}


	}//configure


}


fun main() {
	connQak.configurator.configure();
}


