package connQak

import org.eclipse.californium.core.CoapClient
import org.eclipse.californium.core.coap.MediaTypeRegistry
import it.unibo.kactor.MsgUtil
import it.unibo.kactor.ApplMessage
import org.eclipse.californium.core.CoapResponse
import connQak.QakConnection


class connQakCoap() {

	var client: CoapClient = CoapClient()
	
	var hostIP : String = "0"
	var hostPort : String = "0"
	var destName : String = ""
	var destCtx : String = ""
	
	fun createConnection(connection : QakConnection) {
		val url =
			"coap://${connection.host}:${connection.port}/${connection.ctxqadest}/${connection.qakdest}"
		System.out.println("connQakCoap | url=${url.toString()}")
		//uriStr: coap://192.168.1.22:8060/ctxdomains/waiter
		//client = CoapClient(  )
		client.uri = url.toString()
		client.setTimeout(1000L)
		//initialCmd: to make console more reactive at the first user cmd
		val respGet = client.get() //CoapResponse
		if (respGet != null)
			System.out.println("connQakCoap | createConnection doing  get | CODE=  ${respGet.code} content=${respGet.getResponseText()}")
		else
			System.out.println("connQakCoap | url=  ${url} FAILURE")
		
		hostIP = connection.host
		hostPort = connection.port
		destName = connection.qakdest
		destCtx = connection.ctxqadest
	}
	
	

	fun forward(msg: ApplMessage) {
		System.out.println("connQakCoap | PUT forward ${msg}  ")
		val respPut = client.put(msg.toString(), MediaTypeRegistry.TEXT_PLAIN)
		System.out.println("connQakCoap | RESPONSE CODE=  ${respPut.code}")
	}

	fun request(msg: ApplMessage) {
		System.out.println("connQakCoap | PUT request ${msg}  ")
		val respPut = client.put(msg.toString(), MediaTypeRegistry.TEXT_PLAIN)
		if (respPut != null)
			System.out.println("connQakCoap | answer= ${respPut.getResponseText()}")

	}

	fun emit(msg: ApplMessage) {
		val url = "coap://$hostIP:$hostPort/$destCtx"
		var client = CoapClient( url )	
		val respPut = client.put(msg.toString(), MediaTypeRegistry.TEXT_PLAIN)
		System.out.println("connQakCoap | PUT emit ${msg} RESPONSE CODE=  ${respPut.code}")
	}
	
	fun reply(msg: ApplMessage){
		System.out.println("connQakCoap | PUT reply ${msg}  ")
		val respPut = client.put(msg.toString(), MediaTypeRegistry.TEXT_PLAIN)
		if (respPut != null)
			System.out.println("connQakCoap | answer= ${respPut.getResponseText()}")
	}

	fun readRep(): String {
		val respGet: CoapResponse = client.get()
		return respGet.getResponseText()
	}
}