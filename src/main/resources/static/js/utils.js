const TableState = {
	CLEAN:   'Clean',
	DIRTY:   'Dirty',
	BUSY:    'Busy',
	UNKNOWN: 'Unknown'
};

Object.freeze(TableState);

const AllowedActions = {
	READY_FOR_ORDER: 1,
	ORDER: 2,
	CONSUME_FINISHED: 3,
	PAY: 4,
	NONE: 5
};

Object.freeze(AllowedActions);
