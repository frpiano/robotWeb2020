var stompClient = null;
var mbotImage = new Image();
mbotImage.src = "/images/mbot.png";
var teacupImage = new Image();
teacupImage.src = "images/teacup.png";
var barmanImage = new Image();
barmanImage.src = "images/barman.png";
var smartbellImage = new Image();
smartbellImage.src = "images/doorbell.png";
var clientImage = new Image();
clientImage.src = "images/client.png";

var barmanIdleImage = new Image();
barmanIdleImage.src = "images/barman_idle.png";
var barmanPrepareImage = new Image();
barmanPrepareImage.src = "images/barman_prepare.png";
var barmanReadyImage = new Image();
barmanReadyImage.src = "images/barman_ready.png";

var barmanStateImages = [];

barmanStateImages[0] = barmanIdleImage;
barmanStateImages[1] = barmanPrepareImage;
barmanStateImages[2] = barmanReadyImage;

var trafficLightGreenImage = new Image();
trafficLightGreenImage.src = "images/traffic_light_green.png";
var trafficLightRedImage = new Image();
trafficLightRedImage.src = "images/traffic_light_red.png";

var trafficLightImages = [];
trafficLightImages[0] = trafficLightGreenImage;
trafficLightImages[1] = trafficLightRedImage;

var orderState = [];
var clientIdByTableIds = [];
var targetCoords = [];

var lastBarmanState = 0;
var lastWaitingClient = false;
var lastWaitingClientAtExit = false;

var menuFeesMap = new Map();

let mapStatus;
let cleanedRoomMap;


function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/it-unibo-iss');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/topic/waiter', function (msg) {

            const messageContent = JSON.parse(msg.body).content;
            const currentState = JSON.parse(messageContent);
            onCurrentStateUpdate(currentState);
        });
        stompClient.subscribe('/topic/smartbell', function (msg) {
            // console.log(msg);
            const messageContent = JSON.parse(msg.body).content;
            const smartBellStatus = JSON.parse(messageContent);
            showSmartbellStatus(smartBellStatus);
        });

        stompClient.subscribe('/topic/waiterwalker', function (msg) {
            // console.log(msg);
            const messageContent = JSON.parse(msg.body).content;
            var mapStatus = JSON.parse(messageContent);
            cleanedRoomMap = mapStatus.roomMap.replace(/ /g, "");
            cleanedRoomMap = cleanedRoomMap.replace(/,/g, "");
            onMapStatusUpdate();
        });
    });
}

// mostra lo smartbellStatus sulla pagina web
function showSmartbellStatus(smartbellStatus) {
    var tempText = "OK";
    let styleProps;
    if (smartbellStatus.checkTemperature.includes("NEGATIVE")) {
        tempText = "FLU";
        styleProps = {"background-color": "Red"};
    } else if (smartbellStatus.checkTemperature.includes("NO_CHECK")) {
        tempText = "---";
        styleProps = {"background-color": "White"};
    } else {
        styleProps = {"background-color": "GreenYellow"};
    }

    $("#trafficImg").css(styleProps);

    if (smartbellStatus.timeToWait > 0) {
        $("#timeToWaitLabel").text(smartbellStatus.timeToWait);
    } else {
        $("#timeToWaitLabel").text("---");
    }
}
/*
 * Inserisce nella pagina i dati provenienti dal waiter
 * riguardanti il CurrentState della tearoom
 * */
function onCurrentStateUpdate(currentState) {
    let eventDescription = currentState.description;

    $("#waiterStateTearoomState").text(eventDescription);
    $("#waiterState").text(currentState.waiterState);
    $("#waiterPosX").text(currentState.waiterPosX);
    $("#waiterPosY").text(currentState.waiterPosY);

    let allowedActions = currentState.allowedActions;
    enableButtons(1, allowedActions[1]);
    enableButtons(2, allowedActions[2]);

    let tableMap = currentState.tableMap;
    targetCoords[0] = currentState.waiterPosY;
    targetCoords[1] = currentState.waiterPosX + 1;

    if (!jQuery.isEmptyObject(tableMap)) {
        $("#tableStateOne").text(tableMap[1]);
        $("#tableStateTwo").text(tableMap[2]);
    }

    let orderMap = currentState.orderMap;

    if (!jQuery.isEmptyObject(orderMap)) {
        $("#orderStateOne").text(orderMap[1]);
        $("#orderStateTwo").text(orderMap[2]);
    }
    $("#moneyEarned").text(currentState.moneyEarned + "€");
    $("#moneyEarnedTearoomState").text(currentState.moneyEarned + "€");

    if (tableMap[1] == TableState.DIRTY) {
        $("#product1").val($("#product1 option:first").val());
        $("#product1").change();
    }
    if (tableMap[2] == TableState.DIRTY) {
        $("#product2").val($("#product1 option:first").val());
        $("#product2").change();
    }

    let pendingOrdersMap = currentState.pendingOrders;
    updatePendingOrders(pendingOrdersMap);

    let pending = [];
    pending[1] = orderMap[1];
    pending[2] = orderMap[2];
    // console.log("pending=" + pending);

    let barmanState = 0;
    let barmanStateText = "Idle";
    for (let k = 1; k < pending.length; k++) {
        // console.log("Cycling k=" + k + " => value=" + pending[k]);
        if (pending[k] == "Prepare" || pending[k] == "Submit") {
            // console.log("Order[" + k + "] = Prepare");
            barmanState = 1;
            barmanStateText = "Prepare";
        } else if (pending[k] == "Ready" && barmanState != 1) {
            barmanState = 2;
            // console.log("Order[" + k + "] = Ready");
            barmanStateText = "Order Ready";
        }
    }
    $("#barmanState").attr('class', 'barmanState' + barmanState.toString());
    $("#barmanState").text(barmanStateText);
    lastBarmanState = barmanState;
    lastWaitingClient = currentState.waitingClient;

    $("#awClient").text(currentState.waitingClient);
    $("#targetTable").text(currentState.targetTable);
    $("#targetOrder").text(currentState.targetOrder);
    $("#avalaibleTable").text(currentState.availableTables);
    $("#description").text(currentState.description);
    $("#stoppedActionLabel").text(currentState.stoppedAction);

    orderState[0] = currentState.orderedProductByTable[1];
    orderState[1] = currentState.orderedProductByTable[2];

    clientIdByTableIds[0] = currentState.clientIdByTableId[1];
    clientIdByTableIds[1] = currentState.clientIdByTableId[2];

    updateIds(clientIdByTableIds);
    updateTableCommands(clientIdByTableIds);

    let waitingClientAtExit = currentState.waitingClientAtExit;
    lastWaitingClientAtExit = waitingClientAtExit;

    if (cleanedRoomMap != undefined) {
        renderCanvasMap(cleanedRoomMap, currentState.waitingClient, barmanState, waitingClientAtExit);
    }

}
/*
 * Disegna la mappa in base ai dati provenienti dal walker e dal waiter
 * */
function onMapStatusUpdate() {

    renderCanvasMap(cleanedRoomMap, lastWaitingClient, lastBarmanState, lastWaitingClientAtExit);
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}
/*
 * Invia i comandi base al basicrobot, funzioni utili per correggere il robot fisicos
 * */
function sendTheMove(move) {
    console.log("sendTheMove " + move);
    stompClient.send("/app/move", {}, JSON.stringify({
        'name': move
    }));
}

/*
 * Invia al controller i dati per generare i messaggi relativi all'ordinazione
 * */
function sendOrder(tableNumber, msg) {
    let clientIdString = "clientId" + tableNumber.toString();
    let clientId = $("#" + clientIdString).text()
    let tableIdString = "tableId" + tableNumber.toString();
    let tableId = $("#" + tableIdString).val()
    if (msg == "clientOrder") {
        let productString = "#product" + tableNumber.toString() + " option:selected";
        let orderName = $(productString).text();
        stompClient.send("/app/waiter", {}, JSON.stringify({
            'name': msg,
            'clientId': clientId,
            'tableId': tableNumber,
            'orderName': orderName
        }));
    } else {
        stompClient.send("/app/waiter", {}, JSON.stringify({
            'name': msg,
            'clientId': clientId,
            'tableId': tableNumber,
        }));
    }
}
/*
 * Invia al controller i dati per notificare allo smartbell la presenza di un cliente
 * */
function sendNotify() {
    stompClient.send("/app/notify", {}, JSON.stringify({
        'name': 'notify'
    }));
}
/*
 * Invia al controller i dati per generare il messaggio di pagamento
 * */
function sendPayment(tableNumber) {
    let fee = $("#product" + tableNumber.toString()).val()
    stompClient.send("/app/payment", {}, JSON.stringify({
        'name': 'payment',
        'fee': fee
    }));
}

// abilita i bottoni in base alle azioni già effettuate
function enableButtons(tableNumber, allowedAction) {
    // console.log("enableButtons) tableNumber=" + tableNumber + " ;
    // allowedAction= " + allowedAction);
    switch (allowedAction) {
        case AllowedActions.READY_FOR_ORDER:
            enableElementById("readyOrder" + tableNumber);
            enableElementById("consumeFinished" + tableNumber);
            break;
        case AllowedActions.ORDER:
            disableElementById("readyOrder" + tableNumber);
            enableElementById("order" + tableNumber);
            enableElementById("product" + tableNumber);
            break;
        case AllowedActions.CONSUME_FINISHED:
            disableElementById("order" + tableNumber);
            disableElementById("product" + tableNumber);
            enableElementById("consumeFinished" + tableNumber);
            break;
        case AllowedActions.PAY:
            disableElementById("consumeFinished" + tableNumber);
            enableElementById("pay" + tableNumber);
            break;
        case AllowedActions.NONE:
            disableElementById("readyOrder" + tableNumber);
            disableElementById("order" + tableNumber);
            disableElementById("consumeFinished" + tableNumber);
            disableElementById("pay" + tableNumber);
            disableElementById("product" + tableNumber);
            break;
        default:
            console.log("Nothing to enable... Something is wrong");
    }
}
/*
 * Aggiorna la lista degli ordini pendentis
 * */
function updatePendingOrders(pendingOrdersMap) {
    var pendingOrdersString = "";
    var len = Object.keys(pendingOrdersMap).length;
    // console.log("Map) pendingOrdersMap =>" + l + " ; pendingOrdersMap="+
    // pendingOrdersMap);
    if (len > 0) {
        for (var property in pendingOrdersMap) {
            if (typeof (pendingOrdersMap[property]) === 'object') {
                for (var property2 in pendingOrdersMap[property]) {
                    pendingOrdersString = "<p>" + pendingOrdersMap[property][property2] + " for client " + property + " at table " + property2 + "\n" + pendingOrdersString + "</p>";
                }
            }
        }
    } else {
        pendingOrdersString = "No orders";
    }
    $("#pendingOrders").html(pendingOrdersString);
}
/*
 * Disegna il canvas contenente la mappa che rappresenta lo stato della stanza.
 * */
function renderCanvasMap(stringMap, isWaitingClient, barmanState, isWaitingClientAtExit) {
    if (stringMap.includes("r")) {
        let arrayMap = makeArrayMapFromString(stringMap);

        var canvas = document.getElementById("canvasMap");
        var context = canvas.getContext("2d");

        let rows = arrayMap.length
        let cols = arrayMap[0].length

        let cellWidth = canvas.width / cols;
        let cellHeight = canvas.height / rows;

        let [x, y, width, height] = [0, 0, cellWidth, cellHeight];

        let targetColor = "Yellow";
        let colorObstacle = "#188639";
        let colorWall = "Brown";
        let colorFree = "#98c355";
        let colorCleanTable = "Aqua";
        let colorBorder = "SeaGreen";

        let tableState1 = $("#tableStateOne").html()
        let tableState2 = $("#tableStateTwo").html()

        let tableColor1 = getTableColorFromState(tableState1)
        let tableColor2 = getTableColorFromState(tableState2)

        let canvasHeight = arrayMap.length * cellHeight;
        let canvasWidth = arrayMap[0].length * cellWidth;
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        context.strokeStyle = colorBorder;
        // context.lineWidth = 1;
        arrayMap.forEach((row, i) => row.forEach((value, j) => {
            [x, y] = [width * j, height * i];
            context.moveTo(x, y);
            context.lineWidth = 1;
            var color = "";
            var shouldDrawCup = 0;
            var shouldDrawTable = 0;
            if (value == "1") {
                color = colorFree;
            } else if (i == 3 && (j == 3 || j == 5)) {
                color = colorFree;
                if (j == 3) {
                    shouldDrawTable = 1;
                    if (orderState[0] != "Nothing") {
                        shouldDrawCup = 1;
                    }
                } else {
                    shouldDrawTable = 2;
                    if (orderState[1] != "Nothing") {
                        shouldDrawCup = 1;
                    }
                }
            } else if (value == "X" || value == "|") {
                color = colorObstacle;
            } else {
                color = colorFree;
            }
            if (targetCoords[0] == i && targetCoords[1] == j) {
                color = targetColor;
            }

            context.fillStyle = color;
            if (i == 5 && (j == 2 || j == 7)) {
                context.fillRect(x, y, width, height);
                context.fillRect(x, y, width / 4, height);
                context.fillRect(x + width * 3 / 4, y, width / 4, height);
                context.fillStyle = colorFree;
                context.fillRect(x + width * 1 / 4, y, width / 2, height);
            } else {
                context.fillRect(x, y, width, height);
            }
            context.strokeRect(x, y, width, height);

            if ((i == 5 && j == 1) || (i == 0 && j == 6)) {
                let img;
                if (barmanState !== undefined) {
                    img = barmanStateImages[barmanState];
                } else {
                    img = barmanImage;
                }
                console.log("barmanState = " + barmanState);
                if (i == 5 && j == 1) {
                    img = smartbellImage;
                }

                context.drawImage(img, x, y, width, height);
            }

            if (value == "r") {
                context.drawImage(mbotImage, x, y, width, height);
            } else if (shouldDrawTable == 1 || shouldDrawTable == 2) {
                if (shouldDrawTable == 1) {
                    color = tableColor1;
                } else {
                    color = tableColor2;
                }
                var centerX = cellWidth / 2;
                var centerY = cellHeight / 2;
                var radius = cellWidth / 20 * 9;

                context.lineWidth = 3;
                context.beginPath();
                context.arc(x + centerX, y + centerY, radius, 0, 2 * Math.PI, false);
                context.fillStyle = color;
                context.fill();

                context.stroke();

                if (shouldDrawCup == 1) {
                    context.drawImage(teacupImage, x, y, width, height);
                }
            } else if (i == 6 && j == 2 && isWaitingClient === true) {
                context.drawImage(clientImage, x, y, width, height);
            } else if (i == 4 && j == 7 && isWaitingClientAtExit == true) {
                context.drawImage(clientImage, x, y, width, height);
            } else if (i == 4 && j == 8) {
                if (isWaitingClientAtExit == true) {
                    context.drawImage(trafficLightImages[1], x, y, width, height);
                } else {
                    context.drawImage(trafficLightImages[0], x, y, width, height);
                }

            }
        }));
    }
}
/*
 * Restituisce il colore del tavolo in base allo stato
 * */
function getTableColorFromState(tableState) {
    let tableColor = "Pink"
    if (tableState == TableState.DIRTY)
        tableColor = "DarkGoldenRod";
    else if (tableState == TableState.BUSY)
        tableColor = "IndianRed";
    else if (tableState == TableState.CLEAN)
        tableColor = "Aqua";
    return tableColor;
}

/*
 * Riproduce la mappa in un array multidimensionale estraendola dalla stringa.
 * @param {stringMap}: mappa in formato Stringa @return {matrix}: la mappa come
 * rappresentata come array multidimensionale
 */
function makeArrayMapFromString(stringMap) {
    let matrix = [];
    let splitted = stringMap.split("\n")
    let rows = splitted.length
    let cols = splitted[0].length

    for (let i = 0; i < rows; i++) {
        matrix[i] = [];
        for (let j = 0; j < cols; j++) {
            if (splitted[i][j] != "\n")
                matrix[i][j] = splitted[i][j]
        }
    }
    return matrix;
}
/*
 * Mostra gli i clientID assegnati dal waiter sulla pagina
 * */
function updateIds(clientIdByTableId) {
    for (let i = 0; i < clientIdByTableId.length; i++) {
        if (clientIdByTableId[i] !== -1) {
            let id = "clientId" + (i + 1).toString();
            let val = clientIdByTableId[i].toString();
            $("#" + id).text(val);
        }
    }
}

/*
 * Gestisce l'abilitazione/disabilitazione della pulsantiera del tavolo
 * */
function updateTableCommands(idTables) {
    for (let i = 0; i < idTables.length; i++) {
        if (idTables[i] > 0) {
            enableTableCommand(i + 1)
        } else {
            disableTableCommand(i + 1)
        }
    }
}

function enableTableCommand(tableNumber) {
    let table = "tableCommands" + tableNumber.toString()
    // console.log("Should enable " + table)
    $("#" + table).removeClass("input-disabled");
}

function disableTableCommand(tableNumber) {
    let table = "tableCommands" + tableNumber.toString()
    // console.log("Should disable " + table)
    $("#" + table).addClass("input-disabled");
}

function enableElementById(idName) {
    $("#" + idName).removeClass("input-disabled");
}

function disableElementById(idName) {
    $("#" + idName).addClass("input-disabled");
}

/*
 * Valida la presenza di un ordine nella combo degli ordini
 * */
function validateForClientOrder(tableId) {

    let product = $("#product" + tableId);
    let productVal = product.val();
    product.removeClass('is-invalid');

    if (!$.isNumeric(productVal) || productVal == 0) {
        product.addClass('is-invalid');
        return false;
    }

    return true;
}


$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendName();
    });

    // USED BY SOCKET.IO-BASED GUI

    $("#h").click(function () {
        sendTheMove("h")
    });
    $("#w").click(function () {
        sendTheMove("w")
    });
    $("#s").click(function () {
        sendTheMove("s")
    });
    $("#r").click(function () {
        sendTheMove("r")
    });
    $("#l").click(function () {
        sendTheMove("l")
    });
    $("#x").click(function () {
        sendTheMove("x")
    });
    $("#z").click(function () {
        sendTheMove("z")
    });
    $("#p").click(function () {
        sendTheMove("p")
    });

    $("#notifyEntrance").click(function () {
        sendNotify();
    });

    $("#readyOrder1").click(function () {
        let tableId = 1;
        sendOrder(tableId, "readyForOrder");
    });

    $("#order1").click(function () {
    	console.log("order1_Click")
        let tableId = 1;
        if (validateForClientOrder(tableId)) {
            sendOrder(1, "clientOrder");
        }
    });

    $("#consumeFinished1").click(function () {
        let tableId = 1;
        sendOrder(tableId, "consumeFinished");
    });

    $("#pay1").click(function () {
        let tableId = 1;
        sendPayment(tableId);
    });

    $("#readyOrder2").click(function () {
        let tableId = 2;
        sendOrder(tableId, "readyForOrder");
    });

    $("#order2").click(function () {
        let tableId = 2;
        if (validateForClientOrder(tableId)) {
            sendOrder(2, "clientOrder");
        }
    });

    $("#consumeFinished2").click(function () {
        let tableId = 2;
        sendOrder(tableId, "consumeFinished");
    });

    $("#pay2").click(function () {
        let tableId = 2;
        sendPayment(tableId);
    });

});
