package it.unibo.robotWeb2020;

public class RequestMessageForPayment extends RequestMessage {
	
	public RequestMessageForPayment() {
		super();
	}

	private String fee;

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}
}
