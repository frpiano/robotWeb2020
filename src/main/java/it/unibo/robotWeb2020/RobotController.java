package it.unibo.robotWeb2020;
//https://www.baeldung.com/websockets-spring

//https://www.toptal.com/java/stomp-spring-boot-websocket

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import connQak.configurator;
import connQak.connQakCoap;
import connQak.QakConnection;
import domain.Product;
import it.unibo.kactor.ApplMessage;
import it.unibo.kactor.MsgUtil;
import it.unibo.robotWeb2020.domain.ProductItemListBuilder;

@Configuration
@Controller
public class RobotController {

	@Autowired
	ConfigurableApplicationContext context;

	private String htmlPage = "tearoomGui";

	private Set<String> robotMoves = new HashSet<String>();

	private Set<String> clientMoves = new HashSet<String>();

	private connQakCoap connQakWaiter;
	private connQakCoap connQakBasicrobot;
	private connQakCoap connQakTearoom;
	private connQakCoap connQakWaiterwalker;

	private static final boolean DEBUG = false;

	public RobotController() {
		connQak.configurator.configure();
		HashMap<String, QakConnection> actorMap = connQak.configurator.getActorMap();

		htmlPage = connQak.configurator.getDefaultPage();

		robotMoves.addAll(Arrays.asList(new String[] { "w", "s", "h", "r", "l", "z", "x", "p" }));

		clientMoves.addAll(Arrays.asList(new String[] { "notify", "ready", "order", "finished", "payment" }));

		// creiamo le connessioni verso gli attori per gli observer coap
		connQakWaiter = new connQakCoap();
		connQakBasicrobot = new connQakCoap();
		connQakTearoom = new connQakCoap();
		connQakWaiterwalker = new connQakCoap();

		connQakWaiter.createConnection(actorMap.get("waiter"));
		connQakBasicrobot.createConnection(actorMap.get("basicrobot"));
		connQakTearoom.createConnection(actorMap.get("tearoom"));
		connQakWaiterwalker.createConnection(actorMap.get("waiterwalker"));
	}

	/*
	 * Update the page vie socket.io when the application-resource changes. Thanks
	 * to Eugenio Cerulo
	 */
	@Autowired
	SimpMessagingTemplate simpMessagingTemplate;

	@GetMapping("/")
	public String entry(Model viewmodel) {
		viewmodel.addAttribute("arg", "Entry page loaded. Please use the buttons ");
		viewmodel.addAttribute("products", ProductItemListBuilder.fromProductToProductItem(Product.values()));
		peparePageUpdating();
		return htmlPage;
	}

	// Login form
	@RequestMapping("/login.html")
	public String login() {
		return "login.html";
	}

	// Login form with error
	@RequestMapping("/login-error.html")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login.html";
	}

	private void peparePageUpdating() {
		connQakWaiter.getClient().observe(new CoapHandler() {
			@Override
			public void onLoad(CoapResponse response) {
				if (DEBUG) {
					System.out.println("RobotController --> CoapClient changed ->" + response.getResponseText());
				}
				simpMessagingTemplate.convertAndSend(WebSocketConfig.topicForClient,
						new ResourceRep(response.getResponseText()));
			}

			@Override
			public void onError() {
				System.out.println("RobotController --> CoapClient error!");
			}

		});

		connQakTearoom.getClient().observe(new CoapHandler() {
			@Override
			public void onLoad(CoapResponse response) {
				if (DEBUG) {
					System.out.println("RobotController --> CoapSmartbell changed ->" + response.getResponseText());
				}
				simpMessagingTemplate.convertAndSend(WebSocketConfig.topicForSmartbell,
						new ResourceRep(response.getResponseText()));
			}

			@Override
			public void onError() {
				System.out.println("RobotController --> CoapClient smartbell error!");
			}
		});

		connQakWaiterwalker.getClient().observe(new CoapHandler() {
			@Override
			public void onLoad(CoapResponse response) {
				if (DEBUG) {
					System.out.println("RobotController --> CoapWaiterwalker changed ->" + response.getResponseText());
				}
				simpMessagingTemplate.convertAndSend(WebSocketConfig.topicForWaiterwalker,
						new ResourceRep(response.getResponseText()));
			}

			@Override
			public void onError() {
				System.out.println("RobotController --> CoapClient waiterwalker error!");
			}
		});
	}

	/*
	 * INTERACTION WITH THE BUSINESS LOGIC
	 */
	protected void doBusinessJob(String moveName, Model viewmodel) {
		try {
			if (DEBUG) {
				System.out.println("------------------- RobotController doBusinessJob moveName=" + moveName);
			}
			if (moveName.equals("p")) {
				ApplMessage msg = MsgUtil.buildRequest("web", "step", "step(" + configurator.getRobotStep() + ")",
						connQakBasicrobot.getDestName());
				connQakBasicrobot.request(msg);
			} else {
				ApplMessage msg = MsgUtil.buildDispatch("web", "cmd", "cmd(" + moveName + ")",
						connQakBasicrobot.getDestName());
				connQakBasicrobot.forward(msg);
			}
			// WAIT for command completion ...
			Thread.sleep(400); // QUITE A LONG TIME ...
		} catch (Exception e) {
			System.out.println("------------------- RobotController doBusinessJob ERROR=" + e.getMessage());
			// e.printStackTrace();
		}
	}

	@ExceptionHandler
	public ResponseEntity<String> handle(Exception ex) {
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<String>("RobotController ERROR " + ex.getMessage(), responseHeaders,
				HttpStatus.CREATED);
	}

	@MessageMapping("/move")
	public void backtoclient(RequestMessage message) throws Exception {
		doBusinessJob(message.getName(), null);
	}

	@MessageMapping("/notify")
	public void notify(RequestMessage message) throws Exception {
		ApplMessage msg = MsgUtil.buildRequest("web", "notifyEntrance", "notifyEntrance(0)",
				connQakTearoom.getDestName());
		connQakTearoom.request(msg);
	}

	@MessageMapping("/payment")
	public void sendPayment(RequestMessageForPayment message) {
		try {
			StringBuilder payload = new StringBuilder();
			payload.append(message.getName());
			payload.append("(");
			payload.append(message.getFee());
			payload.append(")");
			ApplMessage msg = MsgUtil.buildReply("web", message.getName(), payload.toString(),
					connQakWaiter.getDestName());
			connQakWaiter.reply(msg);

		} catch (Exception e) {
			System.out.println("RobotController impossible to send payment");
		}
	}

	/**
	 * Send message to waiter about order which has same payload
	 * 
	 * @param message : instance of message from the client
	 * @throws Exception
	 */
	@MessageMapping("/waiter")
	public void sendOrder(RequestMessageforOrder message) {

		try {
			ApplMessage msg;
			final String buildedMsg = buildMsgForOrder(message);
			if (message.getName().equals("clientOrder")) {
				msg = MsgUtil.buildReply("web", message.getName(), buildedMsg, connQakWaiter.getDestName());
				connQakWaiter.reply(msg);
			} else {
				msg = MsgUtil.buildDispatch("web", message.getName(), buildedMsg, connQakWaiter.getDestName());
				connQakWaiter.forward(msg);
			}
		} catch (Exception e) {
			System.out.println("RobotController impossible to send order");
		}
	}

	/**
	 * Build message for order
	 * 
	 * @param message istance of message from client Request
	 * @return a string containign the builded msg
	 */
	private String buildMsgForOrder(RequestMessageforOrder message) {
		StringBuilder sb = new StringBuilder();
		sb.append(message.getName());
		sb.append("(");
		if (message.getOrderName() != null && !message.getOrderName().isEmpty()) {
			sb.append(message.getOrderName());
			sb.append(",");
		}
		sb.append(message.getClientId());
		sb.append(",");
		sb.append(message.getTableId());
		sb.append(")");
		return sb.toString();
	}
}
