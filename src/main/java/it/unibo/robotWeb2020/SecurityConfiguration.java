package it.unibo.robotWeb2020;

import org.springframework.context.annotation.Configuration;  
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;  
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration  
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {  

	@Override  
	public void configure(HttpSecurity http) throws Exception {  
	    http  
	    .authorizeRequests()  
        .antMatchers( "/public/**").permitAll()
        .antMatchers("/css/**").permitAll() //Adding this line solved it
        .antMatchers("/js/**").permitAll() //Adding this line solved it
        .antMatchers("/images/**").permitAll() //Adding this line solved it
        .anyRequest().authenticated()  
            .and()  
        .formLogin()  
            .loginPage("/login.html")  
            .failureUrl("/login-error.html")  
            .permitAll();
	}
	
    @Override  
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {  
        auth.inMemoryAuthentication()  
            .withUser("manager")  
            .password("{noop}manager") // Spring Security 5 requires specifying the password storage format  
            .roles("USER");  
    }  
}
