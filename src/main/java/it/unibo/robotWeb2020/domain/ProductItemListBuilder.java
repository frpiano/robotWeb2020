package it.unibo.robotWeb2020.domain;

import java.util.ArrayList;
import java.util.List;

import domain.Product;

public class ProductItemListBuilder {

    public static List<ProductItem> fromProductToProductItem(Product[] products){
    	List<ProductItem> toReturn = new ArrayList<>();
    	
    	for(Product product : products) {
    		if(product != Product.UNKNOWN && product != Product.NONE) {
    			toReturn.add(new ProductItem(product.getValue(), product.getPrice()));
    		}else if (product == Product.NONE) {
    			toReturn.add(new ProductItem("", product.getPrice()));
    		}
    	}
    	
    	return toReturn;
    }
}
