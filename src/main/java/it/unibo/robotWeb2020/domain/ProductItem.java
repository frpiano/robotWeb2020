package it.unibo.robotWeb2020.domain;


public class ProductItem {
    private String name;
    private Float price;

    public ProductItem(String name, Float price) {
        this.name = name;
        this.price = price;
    }

	public String getName() {
		return name;
	}

	public Float getPrice() {
		return price;
	}
}


